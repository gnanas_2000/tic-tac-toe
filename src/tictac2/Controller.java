package tictac2;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

class Controller{
	private List<Player> players = new ArrayList<>();
	private Board gameBoard;
	private Scanner in = new Scanner(System.in);
	private final char DASH = 'x';
	private final char DOT = 'o';
	
	//Create and initialize board and players
	public Controller(){		
		Player player;
		String name;
		gameBoard = new Board();
		gameBoard.initBoard();
		while(players.size()<2){
			System.out.print("Enter player name:");
			name = in.next();
			if(!name.isEmpty()){
				player = new Player(name);
				players.add(player);
			}	
		}
	}
	
	public void playGame(){
		Player currPlayer=players.get(0);
		boolean gameOver = false;
		while(!gameOver && gameBoard.isGridSpaceLeft()){
			int[] move= promptTurn(currPlayer);
			gameBoard.setPos(move[0],move[1], (currPlayer.equals(players.get(0)))?DASH:DOT);
			gameBoard.printBoard();
			if (gameBoard.isGameOver()) {
				System.out.println(currPlayer.getName() + " won!");
				gameOver = true;
			}
			currPlayer = (currPlayer == players.get(0))?players.get(1):players.get(0);
		}
		System.out.println("Game over!");
	}
	
	private int[] promptTurn(Player temp){
		int row=-1, col=-1;
		System.out.println(temp.getName()+ ", please make your choice");
		while (row<0 || col<0) {
			System.out.print("Enter row (1-3): ");
			row = in.nextInt();
			System.out.println("Enter col (1-3): ");
			col = in.nextInt();
			if(row <1 || row>3 || col<1 || col>3 || !gameBoard.isAllowed(row-1, col-1)){
				System.out.println("Invalid position "+row + ", "+ col+". "+temp.getName()+", please try again.");
				row=-1;col=-1;
			}
		}
		return new int[]{row-1,col-1};
			
	}	
	
	public static void main(String[] args){
		Controller c = new Controller();
		c.playGame();
	}
}